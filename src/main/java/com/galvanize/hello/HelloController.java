package com.galvanize.hello;

import org.springframework.web.bind.annotation.*;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;


@RestController
public class HelloController {
    @GetMapping("/hello")
    public String helloWorld(@RequestParam(required = false, defaultValue = "World") String name) {
        return String.format("Hello %s", name);
    }

    @GetMapping("/calculate")
    public int calculate(@RequestParam(required = false) int[] nums, @RequestParam String operator) {
        int result = nums[0];

        switch (operator) {
            case "plus":
                for (int i = 1; i < nums.length; i++) {
                    result += nums[i];
                }
                break;
            case "minus":
                for (int i = 1; i < nums.length; i++) {
                    result -= nums[i];
                }
                break;
            case "multiply":
                for (int i = 1; i < nums.length; i++) {
                    result *= nums[i];
                }
                break;
            case "divide":
                for (int i = 1; i < nums.length; i++) {
                    result /= nums[i];
                }
                break;
        }
        return result;
    }

    @GetMapping("/vowelsCount")
    public int vowelsCount(@RequestParam(required = false, defaultValue = "") String input) {
        String vowels = "aeiou";
        int count = 0;
        for (int i = 0; i < input.length(); i++) {
            if (vowels.indexOf(input.charAt(i)) >= 0) {
                count++;
            }
        }
        return count;
    }

    @PostMapping("/stringReplace")
    public String stringReplace(@RequestBody String input, @RequestParam String oldString, @RequestParam String newString) {
        StringBuilder result = new StringBuilder();

        if (input.indexOf(oldString) >= 0) {
            result.append(input, 0, input.indexOf(oldString));
            result.append(newString);
            result.append(input.substring(input.indexOf(oldString) + oldString.length()));
        }
        return result.toString();
    }


}
