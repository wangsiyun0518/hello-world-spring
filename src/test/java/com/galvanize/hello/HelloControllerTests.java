package com.galvanize.hello;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloController.class)
public class HelloControllerTests {
    @Autowired
    MockMvc mockMvc;
    // when go to /hello, should return "Hello World"
    @Test
    void helloWorld_noArgs_returnHelloWorld() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));
    }

    // when go to /hello?name=Siyun, should return "Hello Siyun"
    @Test
    void helloWorld_withName_returnHelloName() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/hello?name=Siyun"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Siyun"));
    }

    // when go to /calculate?nums=1,2,5&operator=+, should return 8
    @Test
    void calculate_withNumbersAndOperator_returnComputedValue() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/calculate")
                .param("nums", "8, 2, 4")
                .param("operator", "plus"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("14"));
    }

    // when go to /vowelsCount?input=apple, should return 2
    @Test
    void vowelsCount_withString_returnCNumberOfVowels() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/vowelsCount?input=apple"))
                .andExpect(status().isOk())
                .andExpect(content().json("2"));
    }

    // when send post request with body "apple strawberry blueberry" to /stringReplace?old=apple&new=banana, should return "banana strawberry blueberry"
    @Test
    void stringReplace_withString_returnReplacedString() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/stringReplace?oldString=apple&newString=banana")
                .content("apple strawberry blueberry"))
                .andExpect(status().isOk())
                .andExpect(content().string("banana strawberry blueberry"));
    }
}
